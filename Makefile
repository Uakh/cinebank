JC = javac
SRCDIR = src/
BLDDIR = build/
CFLAGS = -g -d $(BLDDIR)
JAR = jar
JARFILE = Cinebank.jar
MANIFEST = Manifest.mf
JARFLAGS = -cfm $(JARFILE) $(MANIFEST) $(BLDDIR)*.class
.PHONY = all clean help class-files

all: class-files
	$(JAR) $(JARFLAGS)
	
class-files:
	-mkdir -p $(BLDDIR)
	$(JC) $(CFLAGS) $(SRCDIR)*.java

clean:
	-rm -f $(JARFILE)
	-rm -f $(BLDDIR)*.class
	-rm -f *~
	-rm -f $(SRCDIR)*~

help:
	@echo "help: Prints this help"
	@echo "all: builds all class files and makes an executable JAR"
	@echo "class-files: only builds class files"
	@echo "clean: removes all backup/compiled files"
