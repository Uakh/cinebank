import java.util.Arrays;

class Realisateur{

	private String nom;
	private String prenom;
	private Film[] lesFilms;

	public Realisateur(String prenom, String nom){
		this.prenom = prenom;
		this.nom = nom;
		this.lesFilms = new Film[0];
	}

	public String getPrenom(){
		return prenom;
	}

	public String getNom(){
		return nom;
	}

	public Film[] getLesFilms(){
		return lesFilms;
	}

	public void setPrenom(String prenom){
		this.prenom = prenom;
	}

	public void setNom(String nom){
		this.nom = nom;
	}

	public void ajouterUnFilm(Film film){
		int length = lesFilms.length;
		for (int i = 0; i < length; i++){
			if (lesFilms[i].getTitre().equals(film.getTitre())){
				return;
			}
		}
		Film[] lesFilms = Arrays.copyOf(this.lesFilms, length + 1);
		lesFilms[length] = film;
		this.lesFilms = lesFilms;
		film.ajouterUnRealisateur(this);
		return;
	}

	public void supprimerUnFilm(Film film){
		int length = lesFilms.length;
		for (int i = 0; i < length; i++){
			if (lesFilms[i].getTitre().equals(film.getTitre())){
				lesFilms[i] = lesFilms[length - 1];
				Film[] lesFilms = Arrays.copyOf(this.lesFilms, length - 1);
				this.lesFilms = lesFilms;
				film.supprimerUnRealisateur(this);
				return;
			}
		}
		return;
	}
}
