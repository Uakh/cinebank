
/**
 *
 * @author ludovic.devillers
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //Créér Deux clients
	Client client1 = new Client("foo", "bar");
	Client client2 = new Client("lorem", "ipsum");
        
        //Créer six acteurs        
//        JohnTravolta
//        Uma Thurman
//        Samuel L.Jackson
//        LeonardoDiCaprio
        Acteur acteur1 = new Acteur("John", "Travolta");
	Acteur acteur2 = new Acteur("Uma", "Thurman");
	Acteur acteur3 = new Acteur("Samuel L.", "Jackson");
	Acteur acteur4 = new Acteur("Leonardo", "DiCaprio");
	Acteur acteur5 = new Acteur("lorem", "ipsum");
	Acteur acteur6 = new Acteur("fizz", "buzz");
        //Créer deux réalisateurs
//        Quentin Tarantino
//        ChristopherNolan
	Realisateur realisateur1 = new Realisateur("Quentin", "Tarantino");
	Realisateur realisateur2 = new Realisateur("Christopher", "Nolan");
        
        //Créer trois films
//        Django Unchained
//        Pulp Fiction
//        Inception
        
	Film film1 = new Film("Django Unchained");
	Film film2 = new Film("Pulp Fiction");
	Film film3 = new Film("Inception");
        //Associer les films avec leur Acteurs 
  
        film1.ajouterUnActeur(acteur1);
	film2.ajouterUnActeur(acteur2);
	film3.ajouterUnActeur(acteur3);
	film1.ajouterUnActeur(acteur4);
	film2.ajouterUnActeur(acteur5);
	film3.ajouterUnActeur(acteur6);
        
        //Associer les films avec leurs Réalisateurs

        film1.ajouterUnRealisateur(realisateur1);
	film2.ajouterUnRealisateur(realisateur2);
	film3.ajouterUnRealisateur(realisateur2);
        //Faire acheter deux films à un client
       
        Achat achat10 = client1.acheterUnFilm(film1);
	Achat achat11 = client1.acheterUnFilm(film2);
        //Faire Acheter trois films à autre client
        
        Achat achat20 = client2.acheterUnFilm(film1);
	Achat achat21 = client2.acheterUnFilm(film2);
	Achat achat22 = client2.acheterUnFilm(film3);
        
        //Afficher le jour, le mois, année de l'achat d'un film par un client
//        System.out.println();
        
	String jour = Integer.toString(achat10.getJour());
	String mois = Integer.toString(achat10.getMois());
	String annee = Integer.toString(achat10.getAnnee());
	System.out.println(jour + "/" + mois + "/" + annee);
        //Afficher tous le titre de tous les films achetés par un client
       
        for (int i = 0; i < client1.getLesAchats().length; i++){
		System.out.println(client1.getLesAchats()[i].getFilm().getTitre());
	}
        //Afficher le prenom et nom de tous les acteurs de tous les films achetés par un client
	Acteur[] acteuri;
    	for (int i = 0; i < client2.getLesAchats().length; i++){
		acteuri = client2.getLesAchats()[i].getFilm().getLesActeurs();
		for (int j = 0; j < acteuri.length; j++){
			System.out.println(acteuri[j].getPrenom() + acteuri[j].getNom());
		}
	}
    }
    
}
