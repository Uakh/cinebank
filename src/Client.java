import java.util.Calendar;
import java.util.Arrays;

class Client{

	private String nom;
	private String prenom;
	private Achat[] lesAchats;

	public Client(String nom, String prenom){
		this.nom = nom;
		this.prenom = prenom;
		this.lesAchats = new Achat[0];
	}

	public String getPrenom(){
		return prenom;
	}

	public String getNom(){
		return nom;
	}

	public Achat[] getLesAchats(){
		return lesAchats;
	}

	public void setPrenom(String prenom){
		this.prenom = prenom;
	}

	public void setNom(String nom){
		this.nom = nom;
	}

	public Achat acheterUnFilm(Film filmAchete){
		Calendar today = Calendar.getInstance();
		int day = today.get(today.DAY_OF_MONTH);
		int month = today.get(today.MONTH) + 1;
		int year = today.get(today.YEAR);
		Achat achat = new Achat(this, filmAchete, day, month, year);
		int length = this.lesAchats.length;
		Achat[] lesAchats = Arrays.copyOf(this.lesAchats, length + 1);
		lesAchats[length] = achat;
		this.lesAchats = lesAchats;
		return achat;
	}
}
