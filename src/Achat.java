class Achat{

	private Client client;
	private Film film;
	private int jour;
	private int mois;
	private int annee;

	public Achat(Client client, Film film, int jour, int mois, int annee){
		this.client = client;
		this.film = film;
		this.jour = jour;
		this.mois = mois;
		this.annee = annee;
	}

	public Client getClient(){
		return client;
	}

	public Film getFilm(){
		return film;
	}

	public int getJour(){
		return jour;
	}

	public int getMois(){
		return mois;
	}

	public int getAnnee(){
		return annee;
	}
}
