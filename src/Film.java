import java.util.Arrays;

class Film{
	String titre;
	int annee;
	String description;
	Acteur[] lesActeurs;
	Realisateur[] lesRealisateurs;

	public Film(String titre){
		this.titre = titre;
		this.lesActeurs = new Acteur[0];
		this.lesRealisateurs = new Realisateur[0];
	}

	public String getTitre(){
		return titre;
	}

	public int getAnnee(){
		return annee;
	}

	public String getDescription(){
		return description;
	}

	public Acteur[] getLesActeurs(){
		return lesActeurs;
	}

	public Realisateur[] getLesRealisateurs(){
		return lesRealisateurs;
	}

	public void setTitre(String titre){
		this.titre = titre;
	}

	public void setAnnee(int annee){
		this.annee = annee;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public void ajouterUnActeur(Acteur acteur){
		int length = lesActeurs.length;
		for (int i = 0; i < length; i++){
			if (lesActeurs[i].getNom().equals(acteur.getNom())
				&& lesActeurs[i].getPrenom().equals(acteur.getPrenom())){
			return;
			}
		}
		Acteur[] lesActeurs = Arrays.copyOf(this.lesActeurs, length + 1);
		lesActeurs[length] = acteur;
		this.lesActeurs = lesActeurs;
		acteur.ajouterUnFilm(this);
		return;
	}

	public void ajouterUnRealisateur(Realisateur realisateur){
		int length = lesRealisateurs.length;
		for (int i = 0; i < length; i++){
			if (lesRealisateurs[i].getNom().equals(realisateur.getNom())
		&& lesRealisateurs[i].getPrenom().equals(realisateur.getPrenom())){
				return;
			}
		}
		Realisateur[] lesRealisateurs = Arrays.copyOf(this.lesRealisateurs,
							length + 1);
		lesRealisateurs[length] = realisateur;
		this.lesRealisateurs = lesRealisateurs;
		realisateur.ajouterUnFilm(this);
		return;
	}

	public void supprimerUnActeur(Acteur acteur){
		int length = lesActeurs.length;
		for (int i = 0; i < length; i++){
			if (lesActeurs[i].getNom().equals(acteur.getNom())
		&& lesActeurs[i].getPrenom().equals(acteur.getPrenom())){
				lesActeurs[i] = lesActeurs[length - 1];
				Acteur[] lesActeurs = Arrays.copyOf(this.lesActeurs, length - 1);
				this.lesActeurs = lesActeurs;
				acteur.supprimerUnFilm(this);
				return;
			}
		}
		return;
	}

	public void supprimerUnRealisateur(Realisateur realisateur){
		int length = lesRealisateurs.length;
		for (int i = 0; i < length; i++){
			if (lesRealisateurs[i].getNom().equals(realisateur.getNom())
		&& lesRealisateurs[i].getPrenom().equals(realisateur.getPrenom())){
				lesRealisateurs[i] = lesRealisateurs[length - 1];
				Realisateur[] lesRealisateurs = Arrays.copyOf(this.lesRealisateurs, length - 1);
				this.lesRealisateurs = lesRealisateurs;
				realisateur.supprimerUnFilm(this);
				return;
			}
		}
		return;
	}
}
